<?php

return [
    'config' => [
        'mng' => [
            'link',
            'username',
            'password'
        ],
        'surat' => [
            'link',
            'username',
            'password'
        ],
        'aras' => [
            'link',
            'configuration_id',
            'customer_code',
            'main_service_code',
            'username',
            'password'
        ]
    ],
    'params' => [
        'create' => [
            'order_code',
            'barcode',
            'waybill_number',
            'total_price',
            'payment_type',
            'type',
            'invoice_number',
            'integration_code',
            'sender_address_id',
            'sender_name',
            'sender_alternative',
            'sender_city',
            'sender_district',
            'sender_address',
            'sender_town',
            'sender_work_phone',
            'sender_home_phone',
            'sender_mobile_phone',
            'sender_tax_office',
            'sender_tax_number',
            'receiver_address_id',
            'receiver_name',
            'receiver_alternative',
            'receiver_city',
            'receiver_district',
            'receiver_address',
            'receiver_town',
            'receiver_work_phone',
            'receiver_home_phone',
            'receiver_mobile_phone',
            'receiver_email',
            'receiver_tax_office',
            'receiver_tax_number',
        ],
        'track' => ['invoice_number'],
        'cancel' => ['invoice_number'],
    ]
];
