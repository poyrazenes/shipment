<?php

return [
    'mng' => [
        'link' => '',
        'username' => '',
        'password' => '',
    ],
    'surat' => [
        'link' => '',
        'username' => '',
        'password' => '',
    ],
    'aras' => [
        'link' => [
            'address' => '',
            'track' => '',
            'order' => '',
        ],
        'configuration_id' => '',
        'customer_code' => '',
        'main_service_code' => '',
        'username' => '',
        'password' => '',
    ]
];
