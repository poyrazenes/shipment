<?php

namespace Boreas\Shipment;

use Illuminate\Support\ServiceProvider;

class ShipmentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/shipment.php' => config_path('shipment.php'),
        ], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerShipment();

        $this->app->alias(
            'shipment',
            \Boreas\Shipment\Shipment::class
        );

        $this->mergeConfigFrom(
            __DIR__ . '/config/shipment.php', 'shipment'
        );

        $this->mergeConfigFrom(
            __DIR__ . '/config/validation.php', 'validation'
        );
    }

    private function registerShipment()
    {
        $this->app->singleton('shipment', function () {
            return new Shipment();
        });
    }
}