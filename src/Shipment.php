<?php

namespace Boreas\Shipment;

use Boreas\Shipment\ShipmentException\CompanyNotFoundException;
use Boreas\Shipment\ShipmentException\ConfigValidationException;

class Shipment
{
    private static $class_names = [
        'surat' => 'Surat',
        'aras' => 'Aras',
        'mng' => 'MNG'
    ];

    public static function init($code, $config)
    {
        if (array_key_exists($code, self::$class_names)) {
            $namespace = __NAMESPACE__ . '\\Companies\\';
            $namespace .= self::$class_names[$code];

            $shipment_class = new $namespace($config);

            $validation = $shipment_class->validateParams(
                config('validation.config.' . $code),
                $config
            );

            if ($validation->status) {
                return $shipment_class;
            } else {
                throw new ConfigValidationException($validation->message);
            }

        } else {
            throw new CompanyNotFoundException($code . ' kodlu bir kargo şirketi bulunamadı!');
        }
    }
}
