<?php

namespace Boreas\Shipment\Companies;

class ShipmentBase
{
    protected $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function validateParams($required_params, $input_params)
    {
        $response = new \stdClass();
        $response->status = true;
        $response->message = '';
        foreach ($required_params as $param) {
            if (!isset($input_params[$param])) {
                $response->message .= $param . ' gereklidir. ' . "\n";
                $response->status = false;
            } else {
                if (empty($input_params[$param])) {
                    $response->message .= $param . ' alanı boş olamaz. ' . "\n";
                    $response->status = false;
                }
            }
        }

        return $response;
    }

    protected function shipmentResponse($status = false, $message = '', $data = null)
    {
        return [
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
    }
}
