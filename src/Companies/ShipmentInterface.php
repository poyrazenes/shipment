<?php

namespace Boreas\Shipment\Companies;

interface ShipmentInterface
{
    public function create($params);

    public function cancel($params);

    public function track($params);
}
